// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Project_gitGameMode.h"
#include "Project_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

AProject_gitGameMode::AProject_gitGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
